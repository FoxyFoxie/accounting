# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2017-2021 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

envfile:
	@touch -a ./.envs/.local/.secret

dev.build: envfile ## Build containers.
	docker-compose build

dev.destroy: envfile ## Delete the containers, images, and volumes.
	docker-compose down --volumes --remove-orphans --rmi all

dev.up: envfile ## Bring up all containers.
	docker-compose up -d

dev.%.up: envfile ## Bring up a specific container.
	docker-compose up $*

dev.down: envfile ## Bring down all containers.
	docker-compose down

dev.stop: envfile ## Stop all containers.
	docker-compose stop

dev.%.stop: envfile ## Stop a service. Requires *service* name, not container name; i.e. use `web` for the accounting container.
	docker-compose stop $*

dev.logs: envfile ## See and follow logs of all services.
	docker-compose logs -f

dev.%.logs: envfile ## See logs of a service. Requires *service* name, not container name. i.e. use `web` for the accounting container.
	docker-compose logs -f --tail=500 $*

dev.%.shell: envfile ## Shell into the docker container.
	docker exec -i -t $* /bin/bash
